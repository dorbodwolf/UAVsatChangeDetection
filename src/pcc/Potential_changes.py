#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/7/27 14:07
# @Author  : Deyu.Tian
# @Site    : ChangGuang Satellite
# @File    : Potential_changes.py
# @Software: PyCharm Community Edition
import sys
import os

sys.path.append(os.path.abspath('../img'))
import Image
import Image_process
import Raster_statistics as rasterstat

import numpy as np

sys.path.append(os.path.abspath('../config'))
import Config

sys.path.append(os.path.abspath('../gui'))
import Visulazation as visul
from Visulazation import plt
from Visulazation import OnclickTaker

sys.path.append(os.path.abspath('../geom'))
from Geometry import *

from shapely.geometry import LineString

from collections import namedtuple


def getPCC(sParams):
    """
    GET PCC
    :param imsatpath:
    :param imuavpath:
    :param w:
    :param yuzhi_method:
    :param user_defined_yuzhi:
    :param dth_path:可以为空
    :param pcc_path:
    :param min_area:
    :return:
    """
    imsatpath = sParams[0]
    imuavpath = sParams[1]
    w = sParams[2]
    yuzhi_method = sParams[3]
    yuzhi = 0
    min_area = sParams[4]

    diffarr, coeffs, diff_map_path = diff_map_matrix(imsatpath, imuavpath, w)

    if yuzhi_method == "rosin":
        yuzhi = rosin_method(diffarr, None)
    elif yuzhi_method == "polygon":
        dicts = rasterstat.loop_zonal_stats(Config.samples, diff_map_path)
        # print(dicts[0][1])
        yuzhi = dicts[0][1]
    elif yuzhi_method == "mouse":
        satarr, uavarr = np.moveaxis(Image.img2array(imsatpath), 0, 2),\
                             np.moveaxis(Image.img2array(imuavpath), 0,2)
        fig = plt.figure()
        ax_l = plt.subplot(121)
        ax_l.set_title("This is the ref img of sat, DON'T click on it!")
        ax_l.imshow(satarr)
        ax = plt.subplot(122)
        ax.set_title("Take some click on the lightest damage you can't tolerate!")
        ax.imshow(uavarr)
        taker = OnclickTaker(fig)
        plt.show()
        # print(taker.points[0].x, taker.points[0].y)
        diff_map = Image.img2array(diff_map_path)
        diffs = [diff_map[int(p.x), int(p.y)] for p in taker.points]
        # print(diffs)
        yuzhi = np.mean(diffs)
    else:
        print("yuzhi_method must be 'rosin' or 'mouse' or 'polygon'")
        sys.exit(1)

    dth_path = "{}dth_w_{}_yuzhimethod_{}_yuzhi_{}.tif".format(Config.temp, w, yuzhi_method, yuzhi)
    dth, coeffs = Yuzhi_band(diffarr, coeffs, yuzhi, dth_path)
    print(dth.shape)
    print("Dth saved as tiff")
    labels = Image_process.conn_4_neibour(dth)
    unique, counts = np.unique(labels, return_counts= True)
    print(unique.shape, counts.shape)
    for i in range(len(unique)):
        if counts[i] < min_area:
            labels[labels == i] = 0
    pcc_path = "{}PCC_dth_w_{}_yuzhimethod_{}_yuzhi_{}_minarea_{}.tif"\
        .format(Config.temp, w, yuzhi_method, yuzhi, min_area)
    Image.array2rasterUTM(pcc_path, coeffs, labels)
    print("PCC saved at {}".format(pcc_path))
    return pcc_path


def rosin_method(imgarr, mask):
    # img = Image.img2array(grayimage)
    hist = Image_process.find_and_draw__hist(imgarr)
    print(hist)

    """直接对计算的直方图使用rosins method"""
    p_peak = Point(np.argmax(hist), np.max(hist)) #直方图的峰点A
    p_tail = Point(np.max(imgarr), 0) #直方图的尾部B
    p_mid = p_peak.halfway(p_tail) #直方图峰部和尾部连线的中点C
    p_dx = Point(p_mid.x - p_mid.y / ((p_tail.x - p_peak.x) / (p_peak.y - p_tail.y)), 0) #峰尾连线中点处垂线与x轴相交的点D
    print(p_peak.x, p_peak.y, "", p_tail.x, p_tail.y, "", p_mid.x, p_mid.y, "", p_dx.x, p_dx.y)
    hist_list = []
    for i in range(len(hist)):
        hist_list.append((i, hist[i]))
    hist_curv = LineString(hist_list)
    # print(len(hist_curv.coords))
    perpendicular_line = LineString([(p_mid.x, p_mid.y), (p_dx.x, p_dx.y)])
    # print(hist_curv.intersection(perpendicular_line)[1].x)


    plt.subplot(221), plt.imshow(imgarr, 'gray'), plt.title("different map")
    plt.subplot(222), plt.plot(hist), plt.plot((p_mid.x, hist_curv.intersection(perpendicular_line).x),
                                               (p_mid.y, hist_curv.intersection(perpendicular_line).y)), \
                                                plt.title("histgram of different map with rosins' threshood")
    plt.show()
    return hist_curv.intersection(perpendicular_line).x
    # """对直方图进行压缩后使用rosins method"""
    # hist = hist / 1000
    # p_peak = Point(np.argmax(hist), np.max(hist)) #直方图的峰点A
    # p_mid = p_peak.halfway(p_tail)  # 直方图峰部和尾部连线的中点C
    # p_dx = Point(p_mid.x - p_mid.y / ((p_tail.x - p_peak.x) / (p_peak.y - p_tail.y)), 0)  # 峰尾连线中点处垂线与x轴相交的点D
    # hist_list = []
    # for i in range(len(hist)):
    #     hist_list.append((i, hist[i]))
    # hist_curv = LineString(hist_list)
    # perpendicular_line = LineString([(p_mid.x, p_mid.y), (p_dx.x, p_dx.y)])
    # print(hist_curv.intersection(perpendicular_line))
    # plt.subplot(212), plt.plot(hist), plt.plot((p_mid.x, hist_curv.intersection(perpendicular_line).x),
    #                                            (p_mid.y, hist_curv.intersection(perpendicular_line).y)),  \
    #                                         plt.title("histgram of 1/1000 different map with rosins' threhood")



def Yuzhi_band(arr, coeffs, yuzhi, outpath):
    """
    get dth
    :param bandpath:
    :param yuzhi:
    :param outpath:
    :return:
    """
    # arr = Image.img2array(bandpath)
    # coeffs =Image.read_tif_metadata(bandpath)
    arr[arr > yuzhi] = 1000
    arr[arr < yuzhi] = 0
    if outpath == "":
        return arr, coeffs
    else:
        Image.array2rasterUTM(outpath, coeffs, arr)
        return arr, coeffs


def normalize_arr(inarr):
    """
    normalize array to [0, 1]
    :param inarr:
    :return:
    """
    return (inarr-np.max(inarr))/(np.max(inarr)-np.min(inarr))


def diff_map_pixel(imsatpath, imuavpath):
    """
    简单的逐像元差异图计算
    优点是效率高速度快，
    缺点是无法避免配准误差带来的椒盐噪声和重影现象
    just pixel by pixel diff map compute
    :param imsatpath:卫星图像的路径
    :param imuavpath:无人机图像的路径
    :return:两幅图像的差值图
    """
    imsatarr, imuavarr = np.moveaxis(Image.img2array(imsatpath), 0, 2), np.moveaxis(Image.img2array(imuavpath), 0, 2)
    imsatarr = imsatarr.astype(np.float32)
    imuavarr = imuavarr.astype(np.float32)

    if imuavarr.shape[0] == imsatarr.shape[0] and imuavarr.shape[1] == \
            imsatarr.shape[1] and imuavarr.shape[2] == imsatarr.shape[2]:
        pass
    else:
        # imuavarr = imuavarr[:, 1:, :]#这里应设置条件判断
        print("IMUAV and IMSAT must have same shape!")
        sys.exit(1)

    print(imsatarr.shape, imuavarr.shape)
    # visul.visul_arr_rgb(imsatarr)
    # visul.visul_arr_rgb(imuavarr)

    # imggt = Image.read_tif_metadata(imsatpath)
    # for i in range(3):
    #     imsatarr[:, :, i] = (imsatarr[:, :, i] - np.min(imsatarr[:, :, i])) / \
    #                         (np.max(imsatarr[:, :, i]) - np.min(imsatarr[:, :, i]))
    #     imuavarr[:, :, i] = (imuavarr[:, :, i] - np.min(imuavarr[:, :, i])) / \
    #                         (np.max(imuavarr[:, :, i]) - np.min(imuavarr[:, :, i]))
        # Image.array2rasterUTM("{}imsatnorm_{}.tif".format(Config.temp, i), imggt, imsatarr[:, :, i])
        # Image.array2rasterUTM("{}imuavnorm_{}.tif".format(Config.temp, i), imggt, imuavarr[:, :, i])
    # visul.visul_arr_rgb(imsatarr)
    # visul.visul_arr_rgb(imuavarr)
    imsat_desc, imuav_desc = descriptor(imsatarr), descriptor(imuavarr)
    print("features of IMUAV and IMSAT have generated successed, "
          "now compute difference maps pixel by pixel!")
    diffarr = np.zeros((imuavarr.shape[0], imuavarr.shape[1]), dtype='f')
    for i in range(0, imuav_desc.shape[0]):
        for j in range(0, imuav_desc.shape[1]):
            diffarr[i, j] = np.linalg.norm(imuav_desc[i, j] - imsat_desc[i, j])
            #print(imuav_desc[i, j], imsat_desc[i, j], imuav_desc[i, j] - imsat_desc[i, j], diffarr[i, j])
    # np.save(os.path.join(Config.data, "diffmap_pixel.npy"), diffarr)
    imggt = Image.read_tif_metadata(imsatpath)
    Image.array2rasterUTM(os.path.join(Config.temp, "diffmap_pixel.tif"), imggt, diffarr)
    return diffarr


def diff_map_matrix(imsatpath, imuavpath, w=0):
    """
    使用矩阵运算提高差异图计算中的W邻域搜索效率
    use matrix to faster computation
    :param imsatpath:卫星图像的路径
    :param imuavpath:无人机图像的路径
    :param w:搜索邻域块的大小
    :return:两幅图像的差值图
    """
    if w == 0 or w % 2 == 0:
        print("use this function like below: \n"
              "diff_map(imsatpath, imuavpath, w)\n need param w of search box,"
              " its must be odd like 1, 3, 5, 7, 9,.etc.")
        sys.exit(1)
    imsatarr, imuavarr = np.moveaxis(Image.img2array(imsatpath), 0, 2), np.moveaxis(Image.img2array(imuavpath), 0, 2)
    # visul.visul_arr_rgb(imsatarr)
    # visul.visul_arr_rgb(imuavarr)
    imsatarr[imsatarr < 0] = 0 #由于卫星的无数据区域设置了-9999的值（16位无符号整型），导致在使用min-max进行数据归一化时候带来噪音，
                                # 使得卫星的值和无人机的值带来不一致的差异，计算的差异图也就完全表现为无人机影像的模样
                                    #  解决：卫星和无人机的无数据都设置为0
    imsatarr = imsatarr.astype(np.float32)
    imuavarr = imuavarr.astype(np.float32)

    if imuavarr.shape[0] == imsatarr.shape[0] and imuavarr.shape[1] == \
            imsatarr.shape[1] and imuavarr.shape[2] == imsatarr.shape[2]:
        pass
    else:
        # imuavarr = imuavarr[:, 1:, :]#这里应设置条件判断
        print("IMUAV and IMSAT must have same shape!")
        sys.exit(1)

    print(imsatarr.shape, imuavarr.shape)

    # imggt = Image.read_tif_metadata(imsatpath)
    # for i in range(3):
    #     imsatarr[:, :, i] = (imsatarr[:, :, i] - np.min(imsatarr[:, :, i])) / \
    #                         (np.max(imsatarr[:, :, i]) - np.min(imsatarr[:, :, i]))
    #     imuavarr[:, :, i] = (imuavarr[:, :, i] - np.min(imuavarr[:, :, i])) / \
    #                         (np.max(imuavarr[:, :, i]) - np.min(imuavarr[:, :, i]))
    #     Image.array2rasterUTM("{}imsatnorm_{}.tif".format(Config.temp, i), imggt, imsatarr[:, :, i])
    #     Image.array2rasterUTM("{}imuavnorm_{}.tif".format(Config.temp, i), imggt, imuavarr[:, :, i])

    # np.save(os.path.join(Config.data, "diffmap_w9.npy"), diffarr)

    # print(imsatarr.min(), imuavarr.max())
    # visul.visul_arr_rgb(imsatarr)
    # visul.visul_arr_rgb(imuavarr)
    """直接将内存中生成的描述器作为下面计算差异图的输出
    下一步应修改为：先将描述器保存到硬盘为.npy文件，之后读取这个.npy文件为numpy数组作为差异图计算的输入
    修改的原因：描述器计算速度慢，出错以后不便于调试
    """
    if (os.path.isfile("{}imsat_desc.npy".format(Config.temp))and
            os.path.isfile("{}imuav_desc.npy".format(Config.temp))
    ):
        print("features of IMUAV and IMSAT have existed!")
        imsat_desc, imuav_desc = np.load("{}imsat_desc.npy".format(Config.temp)), np.load("{}imuav_desc.npy".format(Config.temp))#read npy
        diffarr = np.zeros((imuavarr.shape[0], imuavarr.shape[1]), dtype='f')
        m = int(((w - 1) / 2))  # 处理Python3“/”运算符不能强制类型转换为整型的问题
        for i in range(m, imuav_desc.shape[0] - m):
            for j in range(m, imuav_desc.shape[1] - m):
                xmin, xmax = j - m, j + m + 1
                ymin, ymax = i - m, i + m + 1
                v = imsat_desc[ymin:ymax, xmin:xmax]
                v_2d = v.reshape((v.shape[0] * v.shape[1]), v.shape[2])
                u = imuav_desc[i, j].reshape(1, imuav_desc[i, j].shape[0])
                diffarr[i, j] = np.min(np.linalg.norm(v_2d - u, axis=1))
                # print(v.shape, v_2d.shape, u.shape, diffarr[i, j])
                # print(imsat_desc[i, j], "1/4", imuav_desc[i, j], "half", diffarr[i, j], "done")
        # np.save(os.path.join(Config.data, "diffmap_w9.npy"), diffarr)
        imggt = Image.read_tif_metadata(imsatpath)
        diffmap_path = "{}diffmap_w_{}.tif".format(Config.temp, w)
        Image.array2rasterUTM(diffmap_path, imggt, diffarr)
        return diffarr, imggt, diffmap_path
    else:
        imsat_desc, imuav_desc = descriptor(imsatarr), descriptor(imuavarr)
        print("features of IMUAV and IMSAT have generated successed, "
              "now compute difference maps with matrix ways!")
        np.save(os.path.join(Config.temp, "imsat_desc.npy"), imsat_desc)
        np.save(os.path.join(Config.temp, "imuav_desc.npy"), imuav_desc)
        diffarr = np.zeros((imuavarr.shape[0], imuavarr.shape[1]), dtype='f')
        m = int(((w - 1) / 2))  # 处理Python3“/”运算符不能强制类型转换为整型的问题
        for i in range(m, imuav_desc.shape[0] - m):
            for j in range(m, imuav_desc.shape[1] - m):
                xmin, xmax = j - m, j + m + 1
                ymin, ymax = i - m, i + m + 1
                v = imsat_desc[ymin:ymax, xmin:xmax]
                v_2d = v.reshape((v.shape[0] * v.shape[1]), v.shape[2])
                u = imuav_desc[i, j].reshape(1, imuav_desc[i, j].shape[0])
                diffarr[i, j] = np.min(np.linalg.norm(v_2d - u, axis=1))
                # print(v.shape, v_2d.shape, u.shape, diffarr[i, j])
                # print(imuav_desc[i, j], u, diffarr[i, j])
        imggt = Image.read_tif_metadata(imsatpath)
        # np.save(os.path.join(Config.data, "diffmap_w9.npy"), diffarr)
        diffmap_path = "{}diffmap_w_{}.tif".format(Config.temp, w)
        Image.array2rasterUTM(diffmap_path, imggt, diffarr)
        return diffarr, imggt, diffmap_path


def diff_map(imsatpath, imuavpath, w=0):
    """
    被我抛弃的差异图计算函数
    input imuav and imsat, return different map
    :param imsatpath:
    :param imuavpath:
    :param w:search box in imsat, w must be odd
    :return:
    """
    if w == 0 or w % 2 == 0:
        print("use this function like below: \n"
              "diff_map(imsatpath, imuavpath, w)\n need param w of search box,"
              " its must be odd like 1, 3, 5, 7, 9,.etc.")
        sys.exit(1)
    imsatarr, imuavarr = np.moveaxis(Image.img2array(imsatpath), 0, 2), np.moveaxis(Image.img2array(imuavpath), 0, 2)
    print(imsatarr.shape, imuavarr.shape)
    imsat_desc, imuav_desc = descriptor(imsatarr), descriptor(imuavarr)
    print("descriptors of IMUAV and IMSAT have generated successed, now compute difference maps!")
    diffarr = np.zeros((imuavarr.shape[0], imuavarr.shape[1]), dtype='f')
    for i in range(imuav_desc.shape[0]):
        for j in range(imuav_desc.shape[1]):
            xmin, xmax = int(j-(w-1)/2), int(j+(w-1)/2+1)
            ymin, ymax = int(i-(w-1)/2), int(i+(w-1)/2+1)
            distsset = []
            for x in range(xmin, xmax):
                for y in range(ymin, ymax):
                    if imuav_desc.shape[1] > x > 0 and 0 < y < imuav_desc.shape[0]:
                        distsset.append(np.linalg.norm(imsat_desc[x, y] - imuav_desc[i, j])) #norm1 of vector/matrix
            diffarr[i, j] = min(distsset)
    np.save(os.path.join(Config.data, "diffmap.npy"), diffarr)
    return diffarr


def descriptor(rgbarr):
    """
    描述器生成函数
    input rgbarr, return relative 36d features set
    :param rgbarr:
    :return:
    """
    desc = np.zeros((rgbarr.shape[0], rgbarr.shape[1], 36), dtype='f')
    # print(desc.shape)
    desc[:, :, 0:9] = Image_process.band_9_neibour_layers_ignoreedge(rgbarr[:, :, 0])
    desc[:, :, 9:18] = Image_process.band_9_neibour_layers_ignoreedge(rgbarr[:, :, 1])
    desc[:, :, 18:27] = Image_process.band_9_neibour_layers_ignoreedge(rgbarr[:, :, 2])
    # luminate = np.zeros((rgbarr.shape[0], rgbarr.shape[1]), dtype=np.float32)
    luminate = rgbarr[:, :, 0] * 0.299 + rgbarr[:, :, 1] * 0.587 + rgbarr[:, :, 2] * 0.114
    # visul.visul_arr_rgb(luminate)
    # print("luminate min max is:", np.min(luminate), np.max(luminate), np.std(luminate))
    # desc[:, :, 27:36] = getIG(np.uint8(luminate)) #if( sdepth == CV_16S && ddepth == CV_32F)
    desc[:, :, 27:36] = getIG(luminate)  # if( sdepth == CV_16S && ddepth == CV_32F)
    # print("max of IG is ", np.max(desc[:, :, 27:36]))
    return desc


def getIG(lumimgarr):
    """
    9邻域梯度图层生成函数
    get IG(stacked layers of pixel-and-its 9-neibours' gradients )
    :param lumimgarr:array of Y
    :return:stacked band layers of gradients
    """
    gradient = Image_process.sobel_f(lumimgarr, kernel_size=5)
    return Image_process.band_9_neibour_layers_ignoreedge(gradient)