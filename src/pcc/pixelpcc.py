# -*- coding: utf-8 -*-
"""
==================================
One Class SVM for Satellite Images
==================================

A simple graphical frontend for one class SVM.
To create positive examples click the left mouse button.

all examples are from the same class.

it uses a one-class SVM.

"""
import sys
import os

from sklearn import svm
import numpy as np

from Potential_changes import *
sys.path.append(os.path.abspath('../img'))
import Image

class PixelPCC(object):
    """the implementation of one class svm
    supported by sklearn and libsvm
    """

    def __init__(self, flood_pts, satpath, uavpath, w_pix, a_pix):
        # set default
        self.fld_pts = flood_pts
        self.sat_p = satpath
        self.uav_p = uavpath
        self.w = w_pix
        self.a = a_pix

    def getYuzhi(self, diff_map_path):
        """基于鼠标点击计算阈值
        以便进一步生成Dth"""
        pts = self.fld_pts
        diff_map = Image.img2array(diff_map_path)
        diffs = [diff_map[int(p[0]), int(p[1])] for p in pts]
        # print(diffs)
        yuzhi = np.mean(diffs)
        return yuzhi
    
    def getPCC(self):
        """
        GET PCC
        :param imsatpath:
        :param imuavpath:
        :param w:
        :param yuzhi_method:
        :param user_defined_yuzhi:
        :param dth_path:可以为空
        :param pcc_path:
        :param min_area:
        :return:
        """
        imsatpath = self.sat_p
        imuavpath = self.uav_p
        w = self.w

        # yuzhi = self.yuzhi
        min_area = self.a

        diffarr, coeffs, diff_map_path = diff_map_matrix(imsatpath, imuavpath, w)

        yuzhi = self.getYuzhi(diff_map_path)

        dth_path = "{}dth_w_{}_yuzhi_{}.tif".format(Config.temp, w, yuzhi)
        dth, coeffs = Yuzhi_band(diffarr, coeffs, yuzhi, dth_path)
        print(dth.shape)
        print("Dth saved as tiff")
        labels = Image_process.conn_4_neibour(dth)
        unique, counts = np.unique(labels, return_counts= True)
        print(unique.shape, counts.shape)
        for i in range(len(unique)):
            if counts[i] < min_area:
                labels[labels == i] = 0
        pcc_path = "{}PCC_dth_w_{}_yuzhi_{}_minarea_{}.tif"\
            .format(Config.temp, w, yuzhi, min_area)
        Image.array2rasterUTM(pcc_path, coeffs, labels)
        print("PCC saved at {}".format(pcc_path))
        return pcc_path        