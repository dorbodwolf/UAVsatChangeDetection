# -*- coding: utf-8 -*-
from __future__ import absolute_import

import argparse

from collections import namedtuple

import sys
import os
sys.path.append(os.path.abspath('../pcc'))
print(os.path.abspath('../pcc'))
from Potential_changes import getPCC


def main():
    parser = argparse.ArgumentParser(description="get PCC")
    parser.add_argument("sat", type=str, help="path to satellite image")
    parser.add_argument("uav", type=str, help="path to uav image")
    parser.add_argument("-w", type=int, help="search circul while compute diff map")
    parser.add_argument("-yuzhi_method", type=str, choices=['rosin', 'polygon', 'mouse'], help="select a yuzhi method for diffmap binary")
    parser.add_argument("-min_area", type=int, help="min pixels of a PCC patch")
    args = parser.parse_args()
    SParams = namedtuple('SParams', ['imsatpath', 'imuavpath', 'w', 'yuzhi_method', 'min_area'])
    sParams = SParams._make([args.sat, args.uav, args.w, args.yuzhi_method, args.min_area])
    getPCC(sParams)


if __name__ == '__main__':
    main()