# -*- coding: utf-8 -*-
"""
构建基于Tkinter的用户图形界面GUI入口
@author：田德宇
"""
import sys, os
import numpy as np
from collections import namedtuple

from Visulazation import plt

try:
    import tkinter as Tk
    import tkinter.filedialog as TkFD
except ImportError:
    # Backward compat for Python 2
    import Tkinter as Tk
    import TkFileDialog as TkFD

sys.path.append(os.path.abspath('../config'))
import Config

sys.path.append(os.path.abspath('../img'))
import Image as gdalImage
from raster2polygon import rastr2poly
from polygon2raster import poly2rast
from Raster_statistics import loop_zonal_stats_max as zonal_stats_max

sys.path.append(os.path.abspath('../occ'))
from ocsvm import OneClassSVM

sys.path.append(os.path.abspath('../pcc')) 
from Potential_changes import *
from pixelpcc import PixelPCC

import Visulazation as visul
from Visulazation import plt
from Visulazation import OnclickTaker

# import matplotlib
# matplotlib.use('TkAgg')
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.backends.backend_tkagg import NavigationToolbar2TkAgg
from matplotlib.figure import Figure
from matplotlib.contour import ContourSet
from matplotlib import pyplot as plt

import cv2
from PIL import Image, ImageTk

class Model(object):
    """The Model which hold the data. It implements the
    observable in the observer pattern and notifies the
    registered observers on change event.
    """
    def __init__(self):
        self.observers = []
        self.data = []#分类的训练样本
        self.flood = []#差异计算的洪水样本

    def add_observer(self, observer):
        """Register an observer. """
        self.observers.append(observer)

    def changed(self, event):
        """Notify the observers. """
        for observer in self.observers:
            observer.update(event, self)    
    
    def changed_1(self, event, arr):
        """Notify the observers. """
        for observer in self.observers:
            observer.update_1(event, self, arr)    
    
    def changed_2(self, event, contro):
        """Notify the observers. """
        for observer in self.observers:
            observer.update_2(event, self, contro)    


class Controller(object):
    def __init__(self, model):
        self.model = model
        self.kernel = Tk.IntVar()
        self.yuzhi_method = Tk.IntVar()
        # Whether or not a model has been fitted
        self.fitted = False
        # 洪水样本点击完毕确认
        self.flood_picked = False
        # pcc路径初值设置
        self.pccpath = ""
        # 农田分类结果路径
        self.farm_class_path = ""
    
    def pick_flood(self):
        self.model.changed_2("pick_flood", self)

    def add_flood(self, x, y):
        self.model.flood.append((x, y))
        self.model.changed_2("flood_added", self)

    def pick_samples(self):
        if (os.path.isfile("{}farm_class.tif".format(Config.temp))):
            self.model.changed('notice')
        else: 
            self.model.changed("pick")
    
    def add_example(self, x, y, label):
        self.model.data.append((x, y, label))
        self.model.changed("example_added")

    def fit(self):
        """训练单类支持向量机并在
        数据图框显示农田分类结果"""
        print("fit the model")     
        if (os.path.isfile("{}farm_class.tif".format(Config.temp))):
            """显示分类效果"""
            farm_class_path = "{}farm_class.tif".format(Config.temp)
            self.farm_class_path = farm_class_path
            pred_arr = gdalImage.img2array(farm_class_path)
            self.model.changed_1("fit", pred_arr)
        else:
            # array like this:
            # xdata   ydata   flag
            # 600.93  800.35  1
            # 500.98  700.37  1
            label = np.array(self.model.data)

            satpath = self.satpath.get()

            gamma = float(self.gamma.get())
            coef0 = float(self.coef0.get())
            degree = int(self.degree.get())
            kernel_map = {0: "linear", 2: "rbf", 1: "poly"}
            kernl = kernel_map[self.kernel.get()]

            """fit单类支持向量机"""
            oc_obj = OneClassSVM(kernl, gamma, coef0, degree, label, satpath)
            pred_arr, farm_class_path = oc_obj.fit()
            self.farm_class_path = farm_class_path
            self.fitted = True
            """显示分类效果"""
            self.model.changed_1("fit", pred_arr)
    
    def preDth(self):
        """差异图的阈值计算初始化
        """
        yuzhi_map = {0: "polygon", 1: "mouse", 2: "rosin"}
        yuzhi_method = yuzhi_map[self.yuzhi_method.get()]
        if yuzhi_method == "rosin":
            pass
        elif yuzhi_method == "polygon":
            pass
        elif yuzhi_method == "mouse":
            self.pick_flood()
        else:
            print("yuzhi_method must be 'rosin' or 'mouse' or 'polygon'")
            sys.exit(1)


    def getPCC(self):
        """得到潜在变化组分
        并在数据框绘制
        """
        satpath = self.satpath.get()
        uavpath = self.uavpath.get()
        w_pix = int(self.w.get())
        a_pix = int(self.a.get())

        flood_pts = self.model.flood
        if self.flood_picked == True:
            pcc_obj = PixelPCC(flood_pts, satpath, uavpath, w_pix, a_pix)
            pcc_path = pcc_obj.getPCC()
            self.pccpath = pcc_path
            pcc_arr = gdalImage.img2array(pcc_path)
            self.pccarr = pcc_arr
            pcc_obj = None
            """显示PCC"""
            self.model.changed_1("pcc", pcc_arr)
            pcc_arr = None
    
    def getZai(self):
        """一键提取受灾农田
        """
        # 将PCC转为矢量图斑
        shppath = "{}.shp".format(self.pccpath[0: -4])
        pccpath = self.pccpath
        imggt = gdalImage.read_tif_metadata(pccpath)

        # make mask band
        #使得保存的PCC矢量文件不包括大部分非变化背景区域
        pccarr = gdalImage.img2array(pccpath)
        mskarr = pccarr
        mskarr[mskarr>0] = 1
        mskpath = "{}_msk.tif".format(self.pccpath[0: -4])
        gdalImage.array2raster(mskpath, imggt, mskarr, gdalImage.getSRS(pccpath))
        print("pcc mask saved succeed!")
        
        rastr2poly(pccpath, shppath, mskpath)
        print("PCC矢量文件保存成功")

        #利用PCC矢量斑块对农田分类结果做区域统计，以此保留以农田为主的变化斑块
        # 保存包含农田类别字段的矢量图层为新的矢量文件
        outshp = "{}_zai.shp".format(self.pccpath[0: -4])
        zonal_stats_max(shppath, self.farm_class_path, outshp)
        #灾害矢量图层保存为栅格文件
        zaipath = "{}_zai.tif".format(self.pccpath[0: -4])
        poly2rast(shppath, zaipath, pccpath, "isFarmChan")
        lyr = None
        print("zai栅格图像保存成功")

        #将灾害结果在数据框显示
        zaiArr = gdalImage.img2array(zaipath)
        self.model.changed_1("zai", zaiArr)
        zaiArr = None









class View(object):
    """Test docstring. """
    def __init__(self, root, controller):

        f = Figure()
        # f = plt.figure()
        ax = f.add_subplot(111)
        # ax.set_xticks([])
        # ax.set_yticks([])
        # ax.set_xlim((x_min, x_max))
        # ax.set_ylim((y_min, y_max))
        canvas = FigureCanvasTkAgg(f, master=root)
        canvas.draw()
        canvas.get_tk_widget().pack(side=Tk.BOTTOM, fill=Tk.BOTH, expand=1)
        canvas._tkcanvas.pack(side=Tk.BOTTOM, fill=Tk.BOTH, expand=1)
        canvas.mpl_connect('button_press_event', self.onclick)
        # toolbar = NavigationToolbar2TkAgg(canvas, root)
        # toolbar.update()
        
        self.controllbar = SkyGUI(root, controller, self)
        self.f = f
        self.ax = ax
        self.canvas = canvas
        # self.controller = controller
        self.controller = controller

    def update_flood(self, model, idx):
        x, y = model.data[idx]
        color = 'r'
        self.ax.plot([x], [y], "%so" % color, scalex=0.0, scaley=0.0)

    def update_example(self, model, idx):
        x, y, l = model.data[idx]
        color = 'w'
        if l == 1:
            color = 'w'
        elif l == -1:
            color = 'k'
        self.ax.plot([x], [y], "%so" % color, scalex=0.0, scaley=0.0)
    
    def update(self, event, model):
        ###在数据框显示卫星影像###
        if event == "pick":
            print("开始采集农田样本")
            satarr = np.moveaxis(gdalImage.img2array(self.satpath.get()), 0, 2) 
            self.ax.imshow(satarr)
        ###显示分类结果###
        if event == "fit":
            print("显示单类支持向量机分类结果")
        ###显示鼠标点击位置###
        if event == "example_added": 
            self.update_example(model, -1)
        self.canvas.draw()
        ###农田分类图存在给出提示
        if event == "notice":
            pass
            #导入趣味提示图片
            # self.ax.imshow()

    def update_1(self, event, model, arr):
        if event == "pick":
            print("开始采集农田样本")
            satarr = np.moveaxis(gdalImage.img2array(self.satpath.get()), 0, 2) 
            self.ax.imshow(satarr)
        if event == "fit":
            print("显示单类支持向量机分类结果")
            self.ax.imshow(arr)
        if event == "pcc":
            print("显示PCC")
            self.ax.imshow(arr)
        if event == "zai":
            print("显示最终灾害结果")
            self.ax.imshow(arr)
        self.canvas.draw()
    
    def update_2(self, event, model, contro):
        if event == "pick_flood":
            print("开始采集农田样本")
            uavarr = np.moveaxis(gdalImage.img2array(contro.uavpath.get()), 0, 2) 
            self.ax.imshow(uavarr)
        if event == "flood_added": 
            self.update_flood(model, -1)
        self.canvas.draw()


    def onclick(self, event):
        if event.xdata > 1 and event.ydata > 1:#防止在影像加载之前用户鼠标错误采集信息
            if event.button == 1:
                self.controller.add_example(event.xdata, event.ydata, 1)
                self.controller.add_flood(event.xdata, event.ydata)
                print('x=%d, y=%d, xdata=%f, ydata=%f' %
                    (event.x, event.y, event.xdata, event.ydata))
            elif event.button == 3:
                #点击取消点的选择
                pass
            elif event.button == 2:
                #结束洪水象元选择 方便进一步提取阈值
                controller.flood_picked = True


class SkyGUI:
    def __init__(self, root, controller, view):

        self.rad_but_var = Tk.StringVar()
        self.w_var = Tk.IntVar()
        self.min_area_var = Tk.IntVar()
        
        """
        ##############
        ###打开文件####
        ##############
        """        
        openf = Tk.Frame(root)
        openf['borderwidth'] = 1
        openf['relief'] = 'sunken'

        paths = Tk.Frame(openf)
        satpath = Tk.Frame(paths)
        Tk.Label(satpath, text="卫星影像路径:", anchor="e", width=13).pack(side=Tk.LEFT)
        view.satpath = Tk.StringVar()
        view.satpath.set("")
        self.satfileentry = Tk.Entry(satpath, width=15, textvariable=view.satpath)
        self.satfileentry.pack(side=Tk.LEFT)

        controller.satpath = Tk.StringVar()
        controller.satpath.set("")
        self.tempentry = Tk.Entry(satpath, width=15, textvariable=controller.satpath)
        # self.tempentry.pack(side=Tk.LEFT)
        Tk.Button(satpath, command=self.opensatfile, text='打开..').pack(side=Tk.LEFT)
        satpath.pack()

        controller.uavpath = Tk.StringVar()
        controller.uavpath.set("")
        uavpath = Tk.Frame(paths)
        Tk.Label(uavpath, text="无人机影像路径:", anchor="e", width=13).pack(side=Tk.LEFT)
        self.uavfileentry = Tk.Entry(uavpath, width=15, textvariable=controller.uavpath)
        self.uavfileentry.pack(side=Tk.LEFT)
        Tk.Button(uavpath, command=self.openuavfile, text='打开..').pack(side=Tk.LEFT)
        uavpath.pack()
        paths.pack()

        """
        ########################################
        ### for ocsvm paramater sets############
        ########################################
        """
        
        
        ocsvm = Tk.Frame(root)
        ocsvm['borderwidth'] = 1
        ocsvm['relief'] = 'sunken'
        
        core = Tk.Frame(ocsvm)
        Tk.Label(core, text="SVM核函数:", anchor="e").pack(anchor=Tk.W)
        Tk.Radiobutton(core, text="线性", value=0, variable=controller.kernel, 
            state="active").pack(anchor=Tk.W)
        Tk.Radiobutton(core, text="多边形", value=1, variable=controller.kernel, 
            state="active").pack(anchor=Tk.W)
        Tk.Radiobutton(core, text="RBF", value=2, variable=controller.kernel, 
            state="active").pack(anchor=Tk.W)
        core.pack(side=Tk.LEFT)
        
        controller.gamma = Tk.StringVar()
        controller.gamma.set("0.01")
        ga = Tk.Frame(ocsvm)
        Tk.Label(ga, text="gamma", anchor="e", width=10).pack(side=Tk.LEFT)
        Tk.Entry(ga, width=6, textvariable=controller.gamma).pack(side=Tk.LEFT)
        ga.pack()

        controller.degree = Tk.StringVar()
        controller.degree.set("3")
        deg = Tk.Frame(ocsvm)
        Tk.Label(deg, text="degree", anchor="e", width=10).pack(side=Tk.LEFT)
        Tk.Entry(deg, width=6, textvariable=controller.degree).pack(side=Tk.LEFT)
        deg.pack()

        controller.coef0 = Tk.StringVar()
        controller.coef0.set("0")
        coef = Tk.Frame(ocsvm)
        Tk.Label(coef, text="coef0", anchor="e", width=10).pack(side=Tk.LEFT)
        Tk.Entry(coef, width=6, textvariable=controller.coef0).pack(side=Tk.LEFT)
        coef.pack()

        oc = Tk.Frame(ocsvm)
        Tk.Button(oc, text='农田样本采集', width=15, command=controller.pick_samples).pack(side=Tk.LEFT)
        oc.pack(side=Tk.LEFT)

        get = Tk.Frame(ocsvm)
        Tk.Button(get, text='训练提取农田', width=15, command=controller.fit).pack(side=Tk.LEFT)
        get.pack(side=Tk.LEFT)


        """
        ########################################
        ### for pixelpcc paramater sets#########
        ########################################
        """
        
        
        pcc = Tk.Frame(root)
        pcc['borderwidth'] = 1
        pcc['relief'] = 'sunken'
        
        core = Tk.Frame(pcc)
        Tk.Label(core, text="差异图阈值方法:", anchor="e").pack(anchor=Tk.W)
        Tk.Radiobutton(core, text="矢量文件", value=0, variable=controller.yuzhi_method, 
            state="disabled").pack(anchor=Tk.W)
        Tk.Radiobutton(core, text="鼠标点选", value=1, variable=controller.yuzhi_method, 
            state="active").pack(anchor=Tk.W)
        Tk.Radiobutton(core, text="Rosin阈值方法", value=2, variable=controller.yuzhi_method, 
            state="disabled").pack(anchor=Tk.W)
        core.pack(side=Tk.LEFT)
        
        controller.w = Tk.StringVar()
        controller.w.set("21")
        ga = Tk.Frame(pcc)
        Tk.Label(ga, text="差异图搜索半径", anchor="e", width=10).pack(side=Tk.LEFT)
        Tk.Entry(ga, width=6, textvariable=controller.w).pack(side=Tk.LEFT)
        ga.pack()

        controller.a = Tk.StringVar()
        controller.a.set("30")
        deg = Tk.Frame(pcc)
        Tk.Label(deg, text="最小变化组分", anchor="e", width=10).pack(side=Tk.LEFT)
        Tk.Entry(deg, width=6, textvariable=controller.degree).pack(side=Tk.LEFT)
        deg.pack()

        get = Tk.Frame(pcc)
        Tk.Button(get, text="初始化", width=10, command=controller.preDth).pack(side=Tk.LEFT)
        get.pack(side=Tk.LEFT)

        get = Tk.Frame(pcc)
        Tk.Button(get, text="潜在变化提取", width=15, command=controller.getPCC).pack(side=Tk.LEFT)
        get.pack(side=Tk.LEFT)


        """
        ########################################
        ### zai farm############################
        ########################################
        """
        zai = Tk.Frame(root)
        zai['borderwidth'] = 1
        zai['relief'] = 'sunken'
        get = Tk.Frame(zai)
        Tk.Button(get, text="一键灾区提取", width=15, command=controller.getZai).pack()
        get.pack()


        #################################
        ## finally pack the Frame########
        #################################
        openf.pack(side=Tk.LEFT)
        ocsvm.pack(side=Tk.LEFT)
        pcc.pack(side=Tk.LEFT)
        zai.pack(side=Tk.BOTTOM)

        """
        #绘图区
        """
        self.cvs11 = Tk.Canvas(root, width=150, height=150, background="pink")
        self.cvs12 = Tk.Canvas(root, width=150, height=150, background="pink")
        # self.cvs13 = Tk.Canvas(root, width=150, height=150, background="pink")
        # self.cvs14 = Tk.Canvas(root, width=150, height=150, background="pink")
        self.cvs11.pack(side=Tk.LEFT)
        self.cvs12.pack(side=Tk.LEFT)
        # self.cvs13.pack(side=Tk.LEFT)
        # self.cvs14.pack(side=Tk.LEFT)


    def opensatfile(self):
        filen = TkFD.askopenfilename(initialdir = "/",
            title = "Select file",filetypes = (("geotiff files","*.tif"),("geotiff files","*.tiff")))
        # self.satfilename.delete(0, self.satfilename.END)
        filen1 = filen.replace('/', '\\')

        self.tempentry.insert(0, filen1)
        self.satfileentry.insert(0, filen1)
        
        satarr = np.moveaxis(gdalImage.img2array(filen1), 0, 2)
        
        #PIL Image类无法处理大于8位的图像显示
        if np.max(satarr) > 255:
            satarr1 = satarr.astype('uint8') 

        photo = ImageTk.PhotoImage(image = Image.fromarray(satarr1).resize((150, 150), Image.ANTIALIAS))
        # Add a PhotoImage to the Canvas
        self.cvs11.image_names = photo
        self.cvs11.configure(height = photo.height(), width = photo.width())
        self.cvs11.create_image(0, 0, image=photo, anchor="nw") 


    def openuavfile(self):
        filen = TkFD.askopenfilename(initialdir = "/",
            title = "Select file",filetypes = (("geotiff files","*.tif"),("geotiff files","*.tiff")))
        # self.uavfilename.delete(0, END)
        filen1 = filen.replace('/', '\\')
        self.uavfileentry.insert(0, filen1)
        
        satarr = np.moveaxis(gdalImage.img2array(filen1), 0, 2)
        
        #PIL Image类无法处理大于8位的图像显示
        if np.max(satarr) > 255:
            satarr = satarr.astype('uint8') 

        photo = ImageTk.PhotoImage(image = Image.fromarray(satarr).resize((150, 150), Image.ANTIALIAS))
        # Add a PhotoImage to the Canvas
        self.cvs12.image_names = photo
        self.cvs12.configure(height = photo.height(), width = photo.width())
        self.cvs12.create_image(0, 0, image=photo, anchor="nw") 



model = Model()

root = Tk.Tk()

controller = Controller(model)

root.wm_title("长光卫星航空测量室——农田洪灾淹没检测")
view = View(root, controller)
model.add_observer(view)
# pcc_gui = SkyGUI(root)
# root.geometry("1400x550+10+5")
Tk.mainloop()