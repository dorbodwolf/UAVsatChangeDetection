#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/8/1 17:12
# @Author  : Aries
# @Site    : 
# @File    : Visulazation.py
# @Software: PyCharm Community Edition
import sys
import os

sys.path.append(os.path.abspath('../geom'))
from Geometry import Point

import matplotlib
matplotlib.use('TkAgg')

import matplotlib.pyplot as plt
import numpy as np


class OnclickTaker:
    def __init__(self, fig):
        self.fig = fig
        self.cid = self.fig.canvas.mpl_connect('button_press_event', self)
        self.points = []

    def __call__(self, event):
        fig = self.fig
        print('x=%d, y=%d, xdata=%f, ydata=%f' %
              (event.x, event.y, event.xdata, event.ydata))
        self.points.append(Point(event.xdata, event.ydata))

        plt.plot(event.xdata, event.ydata, 'r+')
        fig.canvas.draw()
        # fig.canvas.mpl_disconnect(self.cid)
        pass



class LineBuilder:
    def __init__(self, line):
        self.line = line
        self.xs = list(line.get_xdata())
        self.ys = list(line.get_ydata())
        self.cid = line.figure.canvas.mpl_connect('button_press_event', self)

    def __call__(self, event):
        print('click', event)
        if event.inaxes!=self.line.axes: return
        self.xs.append(event.xdata)
        self.ys.append(event.ydata)
        self.line.set_data(self.xs, self.ys)
        self.line.figure.canvas.draw()


def visul_arr_rgb(arr):
    """
    输入numpy数组可视化RGB图像
    :param arr:
    :return:
    """
    plt.imshow(arr)  # plotting by columns
    print(np.max(arr), np.min(arr), np.std(arr))
    plt.show()
    pass


def visul_npy(npy):
    """
    读取npy文件来可视化图像
    :param npy:
    :return:
    """
    arr = np.load(npy)
    plt.imshow(arr)  # plotting by columns
    plt.gray()
    print(np.max(arr), np.min(arr), np.std(arr))
    plt.show()
    pass