# -*- coding: utf-8 -*-
"""
==================================
One Class SVM for Satellite Images
==================================

A simple graphical frontend for one class SVM.
To create positive examples click the left mouse button.

all examples are from the same class.

it uses a one-class SVM.

"""
import sys
import os

from sklearn import svm
import numpy as np

sys.path.append(os.path.abspath('../img'))
import Image

sys.path.append(os.path.abspath('../config'))
import Config

class OneClassSVM(object):
    """the implementation of one class svm
    supported by sklearn and libsvm
    """

    def __init__(self, kernl, gamma, coef0, degree, label, satpath):
        # set default
        self.kernl = kernl
        self.gamma = gamma
        self.coef0 = coef0
        self.degree = degree
        self.trn_labl = label
        self.satpath = satpath

    def fit(self):
        # fit the model
        clf = svm.OneClassSVM(kernel=self.kernl, gamma=self.gamma, 
            coef0=self.coef0, degree=self.degree)
        clf.fit(self.image2train())
        y_pred = clf.predict(self.image2pred())        

        """精度计算、测试使用"""
        # count = 0
        # for i in range(len(y_pred)):
        #     if y_pred[i] == 1:
        #         count = count + 1
        # print(len(y_pred), 'y_pred')
        # print(count, 'true')
        # print(len(y_pred)-count, 'false')
        # print(count/len(y_pred), 'score')
        
        return self.pred2gray(y_pred)


    def image2train(self):
        """ function:
        将影像R G B波段值和鼠标点击点位置对应起来
        做成样本"""

        sat_arr = Image.img2array(self.satpath)
        print(sat_arr.shape) # (3, 1334, 1273)
        gt = Image.read_tif_metadata(self.satpath)
        
        ## 制作栅格掩膜
        from shapely import geometry
        pointList = []
        for i in range(self.trn_labl.shape[0]):
            xgeo, ygeo = Image.pos2coors(gt, self.trn_labl[i, :][0], self.trn_labl[i, :][1])
            pointList.append(geometry.Point(xgeo, ygeo))
        poly = geometry.Polygon([[p.x, p.y] for p in pointList])
        # print(poly.wkt)
        msk_arr = Image.trn_msk_mem(self.satpath, poly.wkt)
        print(msk_arr.shape) #(1334, 1273)

        X = []
        ## 整合用于训练的X 
        for i in range(msk_arr.shape[0]):
            for j in range(msk_arr.shape[1]):
                if msk_arr[i, j] != 0:
                    X.append(sat_arr[ :, i, j])
        
        X_arr = np.asarray(X)
        return X_arr

        
    def image2pred(self):
        """ function:
        将影像R G B波段值做成（samples，features）的
        模型输入形式"""

        sat_arr = Image.img2array(self.satpath)
        print(sat_arr.shape) # (3, 1334, 1273)

        X = []
        ## 整合用于训练的X 
        for i in range(sat_arr.shape[1]):
            for j in range(sat_arr.shape[2]):
                X.append(sat_arr[ :, i, j])
        
        X_arr = np.asarray(X)
        return X_arr

    
    def pred2gray(self, y_pred):
        """function:
        预测结果显示为二值图像
        """
        print(y_pred.shape, 'shape of y_pred')

        sat_arr = Image.img2array(self.satpath)
        print(sat_arr.shape) # (3, 1334, 1273)

        pred_arr = np.zeros((sat_arr.shape[1], sat_arr.shape[2]))
        for i in range(sat_arr.shape[1]):
            for j in range(sat_arr.shape[2]):
                pred_arr[i, j] = y_pred[sat_arr.shape[2] * i + j]
        
        imggt = Image.read_tif_metadata(self.satpath)
        farm_class_path = "{}farm_class.tif".format(Config.temp)
        Image.array2rasterUTM(farm_class_path, imggt, pred_arr)
        return pred_arr, farm_class_path









        