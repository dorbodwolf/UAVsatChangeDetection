#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/8/11 14:18
# @Author  : Aries
# @Site    : 
# @File    : Geometry.py
# @Software: PyCharm Community Edition

class Point:
    """Points class for computing geometry algrithoms"""

    def __init__(self, x=0, y=0):
        """create a new Point at x, y """
        self.x = x
        self.y = y
        pass

    def halfway(self, target):
        """get the halfway Point between self and target"""
        mx = (self.x + target.x) / 2
        my = (self.y + target.y) / 2
        return Point(mx, my)