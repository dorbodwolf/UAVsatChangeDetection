#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/10/8 14:28
# @Author  : Deyu Tian
# @Site    : 
# @File    : polygon2raster.py.py
# @Software: PyCharm Community Edition

from osgeo import gdal, ogr, osr


def poly2rast(Shapefile, OutputImage, RefImage, FldName):
    """create shapefile datasource
    from geotiff file
    """
    Shapefile_ds = ogr.Open(Shapefile, 0) #1 代表打开后可 update，默认是0，无法增加字段也无法写入数据
    Shapefile_layer = Shapefile_ds.GetLayer()


    gdalformat = 'GTiff'
    datatype = gdal.GDT_Byte
    ##########################################################
    # Get projection info from reference image
    Image = gdal.Open(RefImage, gdal.GA_ReadOnly)

    # Rasterise
    print("Rasterising shapefile...")
    Output = gdal.GetDriverByName(gdalformat).Create(OutputImage, Image.RasterXSize, Image.RasterYSize, 1, datatype,
                                                     options=['COMPRESS=DEFLATE'])
    Output.SetProjection(Image.GetProjectionRef())
    Output.SetGeoTransform(Image.GetGeoTransform())

    # Write data to band 1
    Band = Output.GetRasterBand(1)
    Band.SetNoDataValue(-99)
    gdal.RasterizeLayer(Output, [1], Shapefile_layer, options=["ATTRIBUTE={}".format(FldName)])

    # Close datasets
    Band = None
    Output = None
    Image = None
    Shapefile = None

    # Build image overviews
    # subprocess.call("gdaladdo --config COMPRESS_OVERVIEW DEFLATE " + OutputImage + " 2 4 8 16 32 64", shell=True)
    print("Done.")