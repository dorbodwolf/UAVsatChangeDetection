#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/8/21 20:58
# @Author  : Deyu Tian
# @Site    : 
# @File    : Spatial_analysis.py
# @Software: PyCharm Community Edition

import gdal, ogr, osr, numpy
import sys

def zonal_stats_max(feat, input_zone_polygon, input_value_raster):

    # Open data
    raster = gdal.Open(input_value_raster)
    shp = ogr.Open(input_zone_polygon)
    lyr = shp.GetLayer()

    # Get raster georeference info
    transform = raster.GetGeoTransform()
    xOrigin = transform[0]
    yOrigin = transform[3]
    pixelWidth = transform[1]
    pixelHeight = transform[5]

    # Reproject vector geometry to same projection as raster
    sourceSR = lyr.GetSpatialRef()
    targetSR = osr.SpatialReference()
    targetSR.ImportFromWkt(raster.GetProjectionRef())
    coordTrans = osr.CoordinateTransformation(sourceSR,targetSR)
    feat_ = lyr.GetNextFeature()#bug
    if feat.GetFID == feat_.GetFID:
        pass
    else:
        # print("feat != feat_")
        pass
    geom = feat.GetGeometryRef()
    geom.Transform(coordTrans)

    # Get extent of feat
    geom = feat.GetGeometryRef()
    if (geom.GetGeometryName() == 'MULTIPOLYGON'):
        count = 0
        pointsX = []; pointsY = []
        for polygon in geom:
            geomInner = geom.GetGeometryRef(count)
            ring = geomInner.GetGeometryRef(0)
            numpoints = ring.GetPointCount()
            for p in range(numpoints):
                    lon, lat, z = ring.GetPoint(p)
                    pointsX.append(lon)
                    pointsY.append(lat)
            count += 1
    elif (geom.GetGeometryName() == 'POLYGON'):
        ring = geom.GetGeometryRef(0)
        numpoints = ring.GetPointCount()
        pointsX = []; pointsY = []
        for p in range(numpoints):
                lon, lat, z = ring.GetPoint(p)
                pointsX.append(lon)
                pointsY.append(lat)

    else:
        sys.exit("ERROR: Geometry needs to be either Polygon or Multipolygon")

    #得到了输入polygon要素的最小外包矩形
    xmin = min(pointsX)
    xmax = max(pointsX)
    ymin = min(pointsY)
    ymax = max(pointsY)

    # Specify offset and rows and columns to read
    xoff = int((xmin - xOrigin)/pixelWidth)
    yoff = int((yOrigin - ymax)/pixelWidth)
    xcount = int((xmax - xmin)/pixelWidth)+1
    ycount = int((ymax - ymin)/pixelWidth)+1

    # Create memory target raster
    target_ds = gdal.GetDriverByName('MEM').Create('', xcount, ycount, 1, gdal.GDT_Byte)
    target_ds.SetGeoTransform((
        xmin, pixelWidth, 0,
        ymax, 0, pixelHeight,
    ))

    # Create for target raster the same projection as for the value raster
    raster_srs = osr.SpatialReference()
    raster_srs.ImportFromWkt(raster.GetProjectionRef())
    target_ds.SetProjection(raster_srs.ExportToWkt())

    # Rasterize zone polygon to raster
    gdal.RasterizeLayer(target_ds, [1], lyr, burn_values=[1]) #polygon外是0

    # Read raster as arrays
    #data raster to zone raster extent
    banddataraster = raster.GetRasterBand(1)
    dataraster = banddataraster.ReadAsArray(xoff, yoff, xcount, ycount).astype(numpy.float)
    #zone raster
    bandmask = target_ds.GetRasterBand(1)
    datamask = bandmask.ReadAsArray(0, 0, xcount, ycount).astype(numpy.float)

    # Mask zone of raster
    zoneraster = numpy.ma.masked_array(dataraster,  numpy.logical_not(datamask))

    # Calculate statistics of zonal raster
    return numpy.max(zoneraster)


def zonal_stats(feat, input_zone_polygon, input_value_raster):

    # Open data
    raster = gdal.Open(input_value_raster)
    shp = ogr.Open(input_zone_polygon)
    lyr = shp.GetLayer()

    # Get raster georeference info
    transform = raster.GetGeoTransform()
    xOrigin = transform[0]
    yOrigin = transform[3]
    pixelWidth = transform[1]
    pixelHeight = transform[5]

    # Reproject vector geometry to same projection as raster
    sourceSR = lyr.GetSpatialRef()
    targetSR = osr.SpatialReference()
    targetSR.ImportFromWkt(raster.GetProjectionRef())
    coordTrans = osr.CoordinateTransformation(sourceSR,targetSR)
    feat_ = lyr.GetNextFeature()#bug
    if feat.GetFID == feat_.GetFID:
        pass
    geom = feat.GetGeometryRef()
    geom.Transform(coordTrans)

    # Get extent of feat
    geom = feat.GetGeometryRef()
    if (geom.GetGeometryName() == 'MULTIPOLYGON'):
        count = 0
        pointsX = []; pointsY = []
        for polygon in geom:
            geomInner = geom.GetGeometryRef(count)
            ring = geomInner.GetGeometryRef(0)
            numpoints = ring.GetPointCount()
            for p in range(numpoints):
                    lon, lat, z = ring.GetPoint(p)
                    pointsX.append(lon)
                    pointsY.append(lat)
            count += 1
    elif (geom.GetGeometryName() == 'POLYGON'):
        ring = geom.GetGeometryRef(0)
        numpoints = ring.GetPointCount()
        pointsX = []; pointsY = []
        for p in range(numpoints):
                lon, lat, z = ring.GetPoint(p)
                pointsX.append(lon)
                pointsY.append(lat)

    else:
        sys.exit("ERROR: Geometry needs to be either Polygon or Multipolygon")

    #得到了输入polygon要素的最小外包矩形
    xmin = min(pointsX)
    xmax = max(pointsX)
    ymin = min(pointsY)
    ymax = max(pointsY)

    # Specify offset and rows and columns to read
    xoff = int((xmin - xOrigin)/pixelWidth)
    yoff = int((yOrigin - ymax)/pixelWidth)
    xcount = int((xmax - xmin)/pixelWidth)+1
    ycount = int((ymax - ymin)/pixelWidth)+1

    # Create memory target raster
    target_ds = gdal.GetDriverByName('MEM').Create('', xcount, ycount, 1, gdal.GDT_Byte)
    target_ds.SetGeoTransform((
        xmin, pixelWidth, 0,
        ymax, 0, pixelHeight,
    ))

    # Create for target raster the same projection as for the value raster
    raster_srs = osr.SpatialReference()
    raster_srs.ImportFromWkt(raster.GetProjectionRef())
    target_ds.SetProjection(raster_srs.ExportToWkt())

    # Rasterize zone polygon to raster
    gdal.RasterizeLayer(target_ds, [1], lyr, burn_values=[1]) #polygon外是0

    # Read raster as arrays
    #data raster to zone raster extent
    banddataraster = raster.GetRasterBand(1)
    dataraster = banddataraster.ReadAsArray(xoff, yoff, xcount, ycount).astype(numpy.float)
    #zone raster
    bandmask = target_ds.GetRasterBand(1)
    datamask = bandmask.ReadAsArray(0, 0, xcount, ycount).astype(numpy.float)

    # Mask zone of raster
    zoneraster = numpy.ma.masked_array(dataraster,  numpy.logical_not(datamask))

    # Calculate statistics of zonal raster
    return numpy.average(zoneraster),numpy.mean(zoneraster),numpy.median(zoneraster),numpy.std(zoneraster),numpy.var(zoneraster)


def loop_zonal_stats(input_zone_polygon, input_value_raster):

    shp = ogr.Open(input_zone_polygon)
    lyr = shp.GetLayer()
    featList = range(lyr.GetFeatureCount())
    statDict = {}

    for FID in featList:
        feat = lyr.GetFeature(FID)
        meanValue = zonal_stats(feat, input_zone_polygon, input_value_raster)
        statDict[FID] = meanValue
    return statDict

def loop_zonal_stats_max(input_zone_polygon, input_value_raster, output):
    """
    区域统计返回区域内最大值，并将值保存回原矢量layer中
    :param input_zone_polygon:
    :param input_value_raster:
    :return:返回更新后的矢量layer
    """
    shp = ogr.Open(input_zone_polygon, 1) #1 代表打开后可 update，默认是0，无法增加字段也无法写入数据
    lyr = shp.GetLayer()
    # newFld = ogr.FieldDefn("isFarmChan", ogr.OFSTInt16)
    # newFld.SetDefault(-99)
    # newFld.SetWidth(24)
    lyr.ResetReading()
    # lyr.CreateField(newFld)
    # featList = range(lyr.GetFeatureCount())
    feat = lyr.GetNextFeature()

    # for FID in featList:
    while feat:
        # feat = lyr.GetFeature(FID)
        maxValue = zonal_stats_max(feat, input_zone_polygon, input_value_raster)
        if maxValue == 1.0:
            feat.SetField("isFarmChan", 1000)
            lyr.SetFeature(feat)
            feat = lyr.GetNextFeature()
            # lyr.SetFeature(feat)
            # lyr.DeleteFeature(FID)
            # lyr.CreateFeature()
        else:
            feat.SetField("isFarmChan", -999)
            lyr.SetFeature(feat)
            feat = lyr.GetNextFeature()
            # lyr.SetFeature(feat)
    print("所有PCC版块排查完毕！")
    pass

    # #############################################################################
    # #   Create output file for the composed polygons.
    #
    # srs = osr.SpatialReference()
    # ds = gdal.Open(input_value_raster)
    # srs.ImportFromWkt(ds.GetProjection())
    #
    # shp_driver = ogr.GetDriverByName('ESRI Shapefile')
    # shp_driver.DeleteDataSource(output)
    #
    # shp_ds = shp_driver.CreateDataSource(output)
    #
    # out_layer = shp_ds.CreateLayer('out', geom_type=ogr.wkbPolygon,
    #                                srs=srs)
    #
    # src_defn = lyr.GetLayerDefn()
    # poly_field_count = src_defn.GetFieldCount()
    # print("field number", poly_field_count)
    #
    # for fld_index in range(poly_field_count):
    #     src_fd = src_defn.GetFieldDefn(fld_index)
    #
    #     fd = ogr.FieldDefn(src_fd.GetName(), src_fd.GetType())
    #     fd.SetWidth(src_fd.GetWidth())
    #     fd.SetPrecision(src_fd.GetPrecision())
    #     out_layer.CreateField(fd)
    # return lyr

# def main(input_zone_polygon, input_value_raster):
#     return loop_zonal_stats(input_zone_polygon, input_value_raster)


# if __name__ == "__main__":

#     #
#     # Returns for each feature a dictionary item (FID) with the statistical values in the following order: Average, Mean, Medain, Standard Deviation, Variance
#     #
#     # example run : $ python grid.py <full-path><output-shapefile-name>.shp xmin xmax ymin ymax gridHeight gridWidth
#     #

#     if len( sys.argv ) != 3:
#         print "[ ERROR ] you must supply two arguments: input-zone-shapefile-name.shp input-value-raster-name.tif "
#         sys.exit( 1 )
#     print 'Returns for each feature a dictionary item (FID) with the statistical values in the following order: Average, Mean, Medain, Standard Deviation, Variance'
#     print main( sys.argv[1], sys.argv[2] )