# -*- coding: utf-8 -*-
"""
Convert Raster to Polygon
"""
from osgeo import gdal,ogr, osr

def rastr2poly(tiffpath, shppath, mskpath = None):
    """create shapefile datasource 
    from geotiff file
    """
    # mapping between gdal type and ogr field type
    type_mapping = { gdal.GDT_Byte: ogr.OFTInteger,
                    gdal.GDT_UInt16: ogr.OFTInteger,   
                    gdal.GDT_Int16: ogr.OFTInteger,    
                    gdal.GDT_UInt32: ogr.OFTInteger,
                    gdal.GDT_Int32: ogr.OFTInteger,
                    gdal.GDT_Float32: ogr.OFTReal,
                    gdal.GDT_Float64: ogr.OFTReal,
                    gdal.GDT_CInt16: ogr.OFTInteger,
                    gdal.GDT_CInt32: ogr.OFTInteger,
                    gdal.GDT_CFloat32: ogr.OFTReal,
                    gdal.GDT_CFloat64: ogr.OFTReal}
    
    # this allows GDAL to throw Python Exceptions
    gdal.UseExceptions()

    print("正在读取tiff...")

    ds = gdal.Open(tiffpath)
    if ds is None:
        raise Exception("tiff影像可能不存在，请检查路径设置是否正确！")
    srcband = ds.GetRasterBand(1)
    # print(srcband.DataType) #6

    srs = osr.SpatialReference()
    srs.ImportFromWkt(ds.GetProjection())

    #set mask band for polygonize
    mskband = None
    if mskpath is None:
        pass
    else:
        mskds = gdal.Open(mskpath)
        mskband = mskds.GetRasterBand(1)


    print("正在创建矢量文件...")
    dst_layername = "Shape"
    drv = ogr.GetDriverByName("ESRI Shapefile")
    dst_ds = drv.CreateDataSource( shppath)
    if dst_ds is None:
        print("矢量文件为空！")
        raise Exception("矢量路径有问题！")
    dst_layer = dst_ds.CreateLayer( dst_layername, srs=srs)
    raster_field = ogr.FieldDefn('PCC_ID', type_mapping[srcband.DataType])
    dst_layer.CreateField( raster_field)
    _field = ogr.FieldDefn('isFarmChan', ogr.OFSTInt16)
    dst_layer.CreateField(_field)

    #2911 -
    #def Polygonize(*args, **kwargs):
    #   2912
    #    """Polygonize(Band srcBand, Band maskBand, Layer outLayer, int iPixValField, char ** options=None, GDALProgressFunc callback=0, void * callback_data=None) -> int"""
    #2913
    #return _gdal.Polygonize(*args, **kwargs)
    gdal.Polygonize( srcband, mskband, dst_layer, 0, [], callback=None)#

    ds = None
    srcband = None
    mskds = None
    mskband = None
    dst_layer = None
    # gdal.Polygonize( srcband, srcband.GetMaskBand(), dst_layer, 0, [], callback=None) #with nodata mask

