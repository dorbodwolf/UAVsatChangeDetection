#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/7/24 16:35
# @Author  : Deyu
# @Site    : 
# @File    : Image.py
# @Software: PyCharm Community Edition

from osgeo import gdal, osr, ogr
import os, sys

sys.path.append(os.path.abspath('../config'))
import Config


def reprojection(geo_dem):
    """
    gdal reprojection 对遥感影像进行重投影
    :param dem:
    :return:
    """
    cmd = "gdalwarp -t_srs '+proj=utm +zone=43N +datum=WGS84' -dstnodata -9999 " \
          "-overwrite {} {}_toUTM.tif".format(geo_dem, geo_dem[:-4])
    os.system(cmd)

def resampling(utm_dem):
    """
    down sampling dem data for better visualize
    对遥感影像进行重采样
    :param utm_dem:
    :return:
    """
    cmd = "gdal_translate -tr 500 500 -r cubic -a_nodata 0 " \
          "-stats {} {}_resamp.tif".format(utm_dem, utm_dem[:-4])
    os.system(cmd)


def img2array(img):
    """
    read dems to array by gdal
    利用GDAL将遥感影像读取为numpy数组
    :param imgfn path of geotiff
    :return narray of geotiff
    """
    img_data = gdal.Open(img)
    img_array = img_data.ReadAsArray()
    return img_array


def wkt2ogrmem(poly_wkb):
    """
    将shapely创建的wkt格式polygon转化为ogr内存矢量图层
    """
    pass

def trn_msk_mem(rastr, msk_wkt):
    """
    训练样本AOI做成和栅格影像同等尺寸的二值栅格掩膜
    其中矢量图层是来自shapely wkt创建的memery图层
    不需要保存shapefile
    gdal > 2.1.0
    """
    # vector_layer = mskshp
    poly_wkt = msk_wkt
    raster_layer = rastr
    target_layer = "{}farm_train_mask.tiff".format(Config.temp)

    # open the raster layer and get its relevant properties
    raster_ds = gdal.Open(raster_layer, gdal.GA_ReadOnly)
    xSize = raster_ds.RasterXSize
    ySize = raster_ds.RasterYSize
    geotransform = raster_ds.GetGeoTransform()
    projection = raster_ds.GetProjection()

    # Create a memory raster to rasterize into.
    target_ds = gdal.GetDriverByName('MEM').Create('', xSize, ySize, 1, gdal.GDT_Byte)
    target_ds.SetGeoTransform(geotransform)
    target_ds.SetProjection(projection)

    # # create the target layer (1 band)
    # driver = gdal.GetDriverByName('GTiff')
    # target_ds = driver.Create(target_layer, xSize, ySize, bands = 1,
    #  eType = gdal.GDT_Byte, options = ["COMPRESS=DEFLATE"])
    # target_ds.SetGeoTransform(geotransform)
    # target_ds.SetProjection(projection)

    """ 基于shapely wkt多边形创建ogr内存矢量图层用于栅格化函数"""
    # Create a memory layer to rasterize from.
    rast_ogr_ds = \
          ogr.GetDriverByName('Memory').CreateDataSource('wrk')
    rast_mem_lyr = rast_ogr_ds.CreateLayer( 'poly', srs=osr.SpatialReference(projection))

    # Add a polygon.
    wkt_geom = poly_wkt
    feat = ogr.Feature(rast_mem_lyr.GetLayerDefn())
    feat.SetGeometryDirectly(ogr.Geometry(wkt = wkt_geom))
    rast_mem_lyr.CreateFeature(feat)
    # rasterize the vector layer into the target one
    gdal.RasterizeLayer(target_ds, [1], rast_mem_lyr, burn_values = [99])
    return target_ds.ReadAsArray()

def trn_msk(rastr, mskshp, msktif):
    """
    训练样本AOI做成和栅格影像同等尺寸的二值栅格掩膜
    gdal > 2.1.0
    """
    vector_layer = mskshp
    raster_layer = rastr
    target_layer = msktif

    # open the raster layer and get its relevant properties
    raster_ds = gdal.Open(raster_layer, gdal.GA_ReadOnly)
    xSize = raster_ds.RasterXSize
    ySize = raster_ds.RasterYSize
    geotransform = raster_ds.GetGeoTransform()
    projection = raster_ds.GetProjection()

    # create the target layer (1 band)
    driver = gdal.GetDriverByName('GTiff')
    target_ds = driver.Create(target_layer, xSize, ySize, bands = 1,
     eType = gdal.GDT_Byte, options = ["COMPRESS=DEFLATE"])
    target_ds.SetGeoTransform(geotransform)
    target_ds.SetProjection(projection)

    # rasterize the vector layer into the target one
    ds = gdal.Rasterize(target_ds, vector_layer, burnValues = [1])

    target_ds = None
    pass

def pos2coors(gt, row, col):
    """
    将影像按行列号转换为投影后的X Y坐标对
    """
    print('raster geotransform coeffs:', gt[0], gt[1], gt[2], gt[3], gt[4], gt[5])
    
    ###############################################
    ###Xgeo = GT(0) + Xpixel*GT(1) + Yline*GT(2)###
    ###Ygeo = GT(3) + Xpixel*GT(4) + Yline*GT(5)###
    ###############################################
    xgeo = gt[0] + row * gt[1] + col * gt[2] #修复行列搞错导致生成掩膜是空的问题
    ygeo = gt[3] + row * gt[4] + col * gt[5]
    return xgeo, ygeo


def getSRS(tifffile):
    """function
    获取输入影像的坐标参考信息
    """
    ds = gdal.Open(tifffile)
    if ds is None:
        raise Exception("tiff影像可能不存在，请检查路径设置是否正确！")
    srs = osr.SpatialReference()
    srs.ImportFromWkt(ds.GetProjection())
    return srs


def read_tif_metadata(tifffile):
    """
    read tiff imggt
    读取遥感影像的空间转换系数
    :param tifffile:
    :return:
    """
    imgds = gdal.Open(tifffile)
    imggt = imgds.GetGeoTransform()
    # print('raster geotransform coeffs:', imggt[0], imggt[1], imggt[2], imggt[3], imggt[4], imggt[5])
    band = imgds.GetRasterBand(1)
    # b = band.ReadAsArray()
    return imggt

def array2raster(newRasterfn, panTransform, array, srs):
    """
    将数组保存为UTM投影的遥感影像
    :param newRasterfn:
    :param panTransform: imggt
    :param array:
    :return:
    """
    cols = array.shape[1]
    rows = array.shape[0]

    driver = gdal.GetDriverByName('GTiff')
    outRaster = driver.Create(newRasterfn, cols, rows, 1, gdal.GDT_Float32)
    outRaster.SetGeoTransform((panTransform[0], panTransform[1], panTransform[2], panTransform[3],
                               panTransform[4], panTransform[5]))
    outband = outRaster.GetRasterBand(1)
    outband.WriteArray(array)
    outRasterSRS = srs
    outRaster.SetProjection(outRasterSRS.ExportToWkt())
    outband.FlushCache()

def array2rasterUTM(newRasterfn, panTransform, array):
    """
    将数组保存为UTM投影的遥感影像
    :param newRasterfn:
    :param panTransform: imggt
    :param array:
    :return:
    """
    cols = array.shape[1]
    rows = array.shape[0]

    driver = gdal.GetDriverByName('GTiff')
    outRaster = driver.Create(newRasterfn, cols, rows, 1, gdal.GDT_Float32)
    outRaster.SetGeoTransform((panTransform[0], panTransform[1], panTransform[2], panTransform[3],
                               panTransform[4], panTransform[5]))
    outband = outRaster.GetRasterBand(1)
    outband.WriteArray(array)
    outRasterSRS = osr.SpatialReference()
    outRasterSRS.ImportFromEPSG(32652) #utm 52n
    outRaster.SetProjection(outRasterSRS.ExportToWkt())
    outband.FlushCache()

def array2rasterwgs84(newRasterfn, panTransform, array):
    """
    将数组保存为WGS-84坐标系下的遥感影像
    :param newRasterfn:
    :param panTransform: imggt
    :param array:
    :return:
    """
    cols = array.shape[1]
    rows = array.shape[0]

    driver = gdal.GetDriverByName('GTiff')
    outRaster = driver.Create(newRasterfn, cols, rows, 1, gdal.GDT_Float32)
    outRaster.SetGeoTransform((panTransform[0], panTransform[1], panTransform[2], panTransform[3],
                               panTransform[4], panTransform[5]))
    outband = outRaster.GetRasterBand(1)
    outband.WriteArray(array)
    outRasterSRS = osr.SpatialReference()
    outRasterSRS.ImportFromEPSG(4326)
    outRaster.SetProjection(outRasterSRS.ExportToWkt())
    outband.FlushCache()

if __name__ == '__main__':
    trn_msk()
    pass